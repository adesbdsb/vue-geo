# Vue Geo

Project based on [vuepack](https://github.com/egoist/vuepack) to try [Vetur](https://github.com/octref/vetur).

Extended to add a [Material UI theme](https://vuematerial.io/) and [Leaflet.js](http://leafletjs.com/)

## Usage

```bash
$ yarn
$ yarn dev
```

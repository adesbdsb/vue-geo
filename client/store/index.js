import Vue from 'vue';
import Vuex from 'vuex';
import vueresource from 'vue-resource';

import {buildQuery, parseResponse} from 'utils/queryBuilder';

Vue.use(Vuex);
Vue.use(vueresource);

const state = {
  data: [],
  layers: [],
  zoom: 9,
  bounds: false
};

const getters = {
  allData: state => state.data
};

const mutations = {
  UPDATE_DATA (state, data) {
    Vue.set(state, 'data', data);
  },
  CLEAR_DATA (state) {
    Vue.set(state, 'data', []);
  },
  ADD_LAYER (state, layer) {
    state.layers.push(layer);
  },
  UPDATE_LAYERS (state, layers) {
    Vue.set(state, 'layers', layers);
  },
  UPDATE_ZOOM (state, zoom) {
    Vue.set(state, 'zoom', zoom);
  },
  UPDATE_BOUNDS (state) {
    Vue.set(state, 'bounds', true);
  },
  RESET_BOUNDS (state) {
    Vue.set(state, 'bounds', false);
  }
};

const actions = {
  updateData (context, click) {
    if (click) {
      context.commit('UPDATE_BOUNDS');
    }
    return new Promise((resolve, reject) => {
      let query = buildQuery(context.state.layers, context.state.zoom);
      Vue.http.post("http://localhost:9200/us_large_cities/_search?size=0", query).then(response => {
        //Translate data
        context.commit('UPDATE_DATA', parseResponse(response.data));
        resolve(response);
      }, error => {
        console.log("An error has occured");
        reject(error);
      });
    });
  },
  clearData ({ commit }) {
    commit('CLEAR_DATA');
  },
  addLayer ({ commit }, layer) {
    let newLayer = {
      id: layer.layer._leaflet_id,
      geometry: layer.layer._bounds,
      shape: "Rectangle"
    };
    commit('ADD_LAYER', newLayer);
  },
  removeLayer ({ commit, state }, ids) {
    let newLayers = [];
    //Loop over every existing layer and build a new array of layers 
    //that haven't been removed.
    state.layers.forEach((layer) => {
      if (ids.indexOf(layer.id.toString()) == -1) {
        newLayers.push(layer);
      }
    });
    commit('UPDATE_LAYERS', newLayers);
  },
  editLayer ( {commit, state }, layers) {
    let array = state.layers;
    //Loop over the current layers and overwrite the ones
    //that have been edited on the UI
    Object.values(layers._layers).forEach((layer) => {
      let index = state.layers.findIndex((el) => {
        return layer._leaflet_id === el.id;
      });
      let newLayer = {
        id: layer._leaflet_id,
        geometry: layer._bounds,
        shape: "Rectangle"
      };
      array[index] = newLayer;
    });
    commit('UPDATE_LAYERS', array);
  },
  updateZoom ( {commit, state, dispatch }, zoom) {
    commit('UPDATE_ZOOM', zoom);

    if (state.data.length > 0) {
      dispatch('updateData');
    }
  }
};

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});

export default store;

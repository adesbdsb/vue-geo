import Vue from 'vue';
import Router from 'vue-router';

//Lazy loading
const Home = () => System.import('../views/Home');
const Search = () => System.import('../views/Search');

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/map',
      component: Home
    },
    {
      path: '/search',
      component: Search
    },
    {
      path: '/',
      redirect: '/map'
    }
  ]
});

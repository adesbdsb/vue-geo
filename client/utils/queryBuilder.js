import bodybuilder from 'bodybuilder';
import geohash from 'geo-hash';

export default function buildQuery (layers, zoom) {
  let vertices = layers.map((layer) => {
    return {
      top_right: [layer.geometry._northEast.lng, layer.geometry._northEast.lat],
      bottom_left: [layer.geometry._southWest.lng, layer.geometry._southWest.lat] 
    };
  });
  let query =  bodybuilder();
  if (vertices.length > 0) {
    query.addQuery('match_all');
    vertices.forEach((vertex) => {
      query.orFilter('geo_bounding_box', 'location', vertex);
    });
  }
  query.aggregation('geohash_grid', 'location', {
    precision: zoom-2
  });

  return query.build();
}

export function parseResponse (data) {
  return data.aggregations.agg_geohash_grid_location.buckets.map((el) => {
      let latlng = geohash.decode(el.key);
    return {
      type: 'Feature',
      properties: {
        name: 'Test',
        count: el.doc_count
      },
      geometry: {
        type: 'Point',
        coordinates: [latlng.lon, latlng.lat]
      }
    };
  });
}

export {
  buildQuery
};
